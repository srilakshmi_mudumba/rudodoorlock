package com.app.rudo.view.lockdetails.passcode

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.contrants.Constants
import com.app.rudo.databinding.FragmentGeneratePasscodeOneTimeBinding
import com.app.rudo.model.locklist.Lock
import com.app.rudo.utils.*
import kotlinx.android.synthetic.main.fragment_generate_passcode_one_time.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class GeneratePasscodeOneTimeFragment(private var lock: Lock) : Fragment(),DialogUitls.OnClickDialogItemImpl, GeneratePasscodeImpl,
    KodeinAware {
    override val kodein: Kodein by kodein()
    private val factory: GeneratePassCodeViewmodelFactory by instance<GeneratePassCodeViewmodelFactory>()
    private var viewModel: GeneratePasscodeViewModel? = null
    private var dialog: AlertDialog? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentGeneratePasscodeOneTimeBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_generate_passcode_one_time, container, false)
        viewModel = ViewModelProvider(this, factory).get(GeneratePasscodeViewModel::class.java)
        binding.viewmodel = viewModel
        viewModel?.registerListner(this)
        viewModel?.setkeyboardPwdType(Constants.Onetime)
        viewModel?.lock = lock
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel?.startTime = DateUtils.getTimeForPasscodeFormart()
        viewModel?.endTime = DateUtils.getTimeForPasscodeFormart()
    }
    override fun onStarted() {
        context?.hideKeyboard(view?.rootView!!)
        progress_bar.show()
    }

    override fun onSuccess(message: String) {
        edtEkeyName.text = null
        progress_bar.hide()

        dialog = showAlertDialog{
            eText.text = message
            btnClickListener{
                dialog?.cancel()
            }
            imgShare.visibility = View.VISIBLE
            shareClickListener {
               // dialog?.dismiss()
                val sendIntent = Intent()
                sendIntent.setAction(Intent.ACTION_SEND)
                // sendIntent.setPackage("com.whatsapp")
                sendIntent.putExtra(Intent.EXTRA_TEXT, message)
                sendIntent.setType("text/plain")
                startActivity(sendIntent)
            }
        }
        dialog?.setCancelable(false)

        dialog?.show()
       // DialogUitls.showDialog(requireContext(),"Alert",message,true,false,this@GeneratePasscodeOneTimeFragment)

    }

    override fun onFailure(message: String) {
        view?.rootView?.snackbar(message)

    }

    override fun onTimeError() {
        requireContext().toast(getString(R.string.time_error))
    }


    override fun onClickYes(dialog: Dialog) {

    }

    override fun onClickNo(dialog: Dialog) {

    }

    override fun onClickDialogDelete(password: String) {

    }


    override fun onClickCancel(dialog: Dialog) {

    }
}