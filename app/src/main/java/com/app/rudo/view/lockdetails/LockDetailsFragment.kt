package com.app.rudo.view.lockdetails

import android.Manifest
import android.app.Dialog
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.app.rudo.R
import com.app.rudo.contrants.Constants
import com.app.rudo.databinding.FragmentLockDetailsBindingImpl
import com.app.rudo.model.PassCodeListModel
import com.app.rudo.model.eaccess.EAccessKeyList
import com.app.rudo.model.ekeys.EAccessKey
import com.app.rudo.model.lockdetails.KeyData
import com.app.rudo.model.locklist.Lock
import com.app.rudo.model.recodes.RecodeDetailsModel
import com.app.rudo.utils.*
import com.bumptech.glide.Glide
import com.ttlock.bl.sdk.api.TTLockClient
import com.ttlock.bl.sdk.callback.*
import com.ttlock.bl.sdk.constant.ControlAction
import com.ttlock.bl.sdk.constant.Feature
import com.ttlock.bl.sdk.constant.LogType
import com.ttlock.bl.sdk.entity.ControlLockResult
import com.ttlock.bl.sdk.entity.LockError
import com.ttlock.bl.sdk.util.SpecialValueUtil
import kotlinx.android.synthetic.main.fragment_lock_details.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import timber.log.Timber
import java.net.URLDecoder

class LockDetailsFragment : Fragment(), LockDeatilsImpl, DialogUitls.OnClickDialogItemImpl,
    KodeinAware {
    private val REQUEST_PERMISSION_REQ_CODE = 11
    override val kodein: Kodein by kodein()
    private val factory: LockDetailsViewmodelFactory by instance<LockDetailsViewmodelFactory>()
    private var viewModel: LockDetailsViewModel? = null
    private var keyData: KeyData? = null
    private var dialog: AlertDialog? = null
    var lock: Lock? = null
    var eAccessKey: EAccessKeyList? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentLockDetailsBindingImpl =
            DataBindingUtil.inflate(inflater, R.layout.fragment_lock_details, container, false)
        viewModel = ViewModelProvider(this, factory).get(LockDetailsViewModel::class.java)
        binding.viewmodel = viewModel
        viewModel?.registerListner(this)
        ensureBluetoothIsEnabled()
        if (viewModel?.lock != null) {
            lock = viewModel?.lock
        } else {
            arguments?.let {
                eAccessKey =
                    (EAccessKeyList::class.java).cast(arguments?.getSerializable("lockData"))
                lock = Lock(
                    0,
                    eAccessKey?.electricQuantity,
                    eAccessKey?.keyboardPwdVersion,
                    eAccessKey?.lockAlias,
                    eAccessKey?.lockId,
                    eAccessKey?.noKeyPwd,
                    eAccessKey?.lockMac,
                    eAccessKey?.lockName,
                    eAccessKey?.specialValue,
                    eAccessKey?.lockData,
                    eAccessKey?.userType,
                    eAccessKey?.keyId
                )
            }
        }
        lock?.let {
            binding.lock = lock
            viewModel?.lock = lock
            viewModel?.setSelectedLockId(lock?.lockId!!)
            // viewModel?.getLockList(lock?.lockId!!)
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startScan()
        featureEnabled()
        onClickImagaLock()
        showBatteryLavel(eAccessKey?.electricQuantity!!)
    }

    override fun onProgressHide() {
        progress_bar.hide()
    }

    override fun onStarted() {

    }

    override fun onSuccess() {

    }

    override fun onFailure(message: String) {
        view?.rootView?.snackbar(message)

    }

    override fun onClickYes(dialog: Dialog) {
        dialog.cancel()
    }

    override fun onClickNo(dialog: Dialog) {
        dialog.cancel()
    }

    override fun onClickDialogDelete(password: String) {

    }


    override fun onClickCancel(dialog: Dialog) {

    }


    override fun onSuccesLockDetails(lockDetail: KeyData) {
        /* keyData = lockDetail
         progress_bar?.hide()
         keyData?.lockAlias?.let {
             val decoded: String = URLDecoder.decode(it, "UTF-8")
             deviceName.text = decoded
             featureEnabled()
         }
 */
    }

    private fun featureEnabled() {
        lock.let {
            val decoded: String = URLDecoder.decode(lock?.lockAlias, "UTF-8")
            deviceName.text = decoded
            if (lock?.userType != null && ( Constants.ADMIN_KEY == lock?.userType ||
                        (Constants.COMMON_KEY == lock?.userType && eAccessKey?.keyRight == 1))) {
                val fp = SpecialValueUtil.isSupportFeature(
                    lock?.specialValue!!,
                    Feature.FINGER_PRINT.toInt()
                )
                val ic = SpecialValueUtil.isSupportFeature(lock?.specialValue!!, Feature.IC.toInt())
                //val pc = SpecialValueUtil.isSupportFeature(lock?.specialValue!!, Feature.PASSCODE.toInt())

                if (fp) {
                    fingerPrint.visibility = View.VISIBLE
                }
                if (ic) {
                    iccardLinerlayout.visibility = View.VISIBLE
                }
            } else {
                llAdminkey.visibility = View.GONE
            }

        }
    }

/*    override fun onClickLockImage() {

    }*/

    private fun onClickImagaLock() {
        lockUnlock.setOnClickListener {
            doLock()
        }
        lockUnlock.setOnLongClickListener {
            doUnLock()
            return@setOnLongClickListener true
        }
    }

    override fun onSuccessLockKey(lockKeyList: List<EAccessKey>) {

    }

    override fun onSuccessPassCodes(lockKeyList: List<PassCodeListModel>) {

    }

    override fun onSuccessUnlockRecords(list: List<RecodeDetailsModel>) {

    }

    override fun onEAccessKeyDeleteSuccess() {
        view?.findNavController()?.navigate(R.id.homeFragment)
    }

    override fun onDeletePasscodeFromDetails(passcodeData: PassCodeListModel) {

    }


    private fun doUnLock() {
        try {
            Glide.with(requireContext())
                .load(R.drawable.animat_lock)
                .into(lockUnlock)
            ensureBluetoothIsEnabled()
            TTLockClient.getDefault().controlLock(
                ControlAction.UNLOCK,
                lock?.lockData,
                lock?.lockMac,
                object : ControlLockCallback {

                    override fun onControlLockSuccess(controlLockResult: ControlLockResult?) {

                        getBattery()
                        Glide.with(requireContext())
                            .load(R.drawable.ic_lock_blue)
                            .into(lockUnlock)
                        dialog = showAlertDialog {
                            txtMessage.visibility = View.GONE
                            eText.text = "Unlock"
                            btnClickListener {
                                if("One-Time" == eAccessKey?.remarks){
                                    viewModel?.deleteeAccessKeyOneTime(eAccessKey?.keyId!!)
                                }
                                getRecords4mLock()
                                dialog?.cancel()
                            }
                        }
                        dialog?.setCancelable(false)
                        dialog?.show()
                    }

                    override fun onFail(error: LockError) {
                        Glide.with(requireContext())
                            .load(R.drawable.ic_lock_blue)
                            .into(lockUnlock)
                        if (error?.name == "LOCK_CONNECT_FAIL") {
                            requireContext().toast(requireContext().getString(R.string.bluetooth_operation_failed))
                        } else {
                            view?.rootView?.snackbar("Unlock Failed")
                        }
                    }
                })
        }catch (e:Exception){

        }
    }

    private fun doLock() {
        try {
            TTLockClient.getDefault().controlLock(
                ControlAction.LOCK,
                lock?.lockData,
                lock?.lockMac,
                object : ControlLockCallback {
                    override fun onControlLockSuccess(controlLockResult: ControlLockResult?) {
                        getBattery()
                        requireContext().toast("Lock is locked")
                    }

                    override fun onFail(error: LockError) {
                        if (error?.name == "LOCK_CONNECT_FAIL") {
                            requireContext().toast(requireContext().getString(R.string.bluetooth_operation_failed))
                        }
                        //Toast.makeText(getActivity(),"lock lock fail!--" + error.getDescription(),Toast.LENGTH_LONG).show();
                    }
                })
        }catch (e:Exception){

        }
    }

    private fun ensureBluetoothIsEnabled() {
        if (!TTLockClient.getDefault().isBLEEnabled(context)) {
            TTLockClient.getDefault().requestBleEnable(activity)
        }
        // setLockTime()

    }

    private fun getLockTime() {
        TTLockClient.getDefault().getLockTime(
            lock?.lockData,
            lock?.lockMac,
            object : GetLockTimeCallback {
                override fun onFail(p0: LockError?) {

                }

                override fun onGetLockTimeSuccess(p0: Long) {
                    Constants.LOCKTIME = p0
                }

            }
        )
    }

    private fun setLockTime() {
        TTLockClient.getDefault().setLockTime(
            System.currentTimeMillis(),
            lock?.lockData,
            lock?.lockMac,
            object : SetLockTimeCallback {
                override fun onSetTimeSuccess() {
                    //makeToast("lock time is corrected")
                    getLockTime()
                }

                override fun onFail(error: LockError?) {
                    // makeErrorToast(error)
                }
            })
    }

    private fun getRecords4mLock() {
        lock?.let {
            TTLockClient.getDefault().getOperationLog(
                LogType.NEW,
                lock?.lockData,
                lock?.lockMac,
                object : GetOperationLogCallback {
                    @RequiresApi(Build.VERSION_CODES.O)
                    override fun onGetLogSuccess(log: String) {
                        //  val encodedString: String = Base64.getEncoder().encodeToString(log.toByteArray())
                        Timber.i("Json Data %s", log)
                        viewModel?.uploadRecords(log)
                    }

                    override fun onFail(error: LockError) {
                        if(error?.name == "LOCK_CONNECT_FAIL"){
                            requireContext().toast(requireContext().getString(R.string.bluetooth_operation_failed))
                        }else {
                            view?.rootView?.snackbar(error.errorMsg)
                        }
                    }
                })
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        /**
         * BT service should be released before Activity finished.
         */
        TTLockClient.getDefault().stopBTService()
    }

    private fun startScan() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_PERMISSION_REQ_CODE
            )
        }
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_PERMISSION_REQ_CODE
            )

        }
    }

    private fun getBattery() {
       // flProgress?.visibility = View.VISIBLE
       // progress_bar?.show()
        try {
            TTLockClient.getDefault().getBatteryLevel(
                lock?.lockData,
                lock?.lockMac,
                object : GetBatteryLevelCallback {
                    override fun onFail(p0: LockError?) {
                      //  progress_bar?.hide()
                       // flProgress?.visibility = View.GONE
                        if(p0?.name == "LOCK_CONNECT_FAIL"){
                            requireContext().toast(requireContext().getString(R.string.bluetooth_operation_failed))
                        }
                    }

                    override fun onGetBatteryLevelSuccess(p0: Int) {
                      //  progress_bar?.hide()
                      //  flProgress?.visibility = View.GONE
                        Timber.i("battery %s",p0)
                        if(p0 != null && p0 >0) {
                            showBatteryLavel(p0)
                            viewModel?.uploadBatteryStatus(lock?.lockId!!, p0)
                        }
                    }
                }
            )
        }catch (e:NullPointerException){
           // progress_bar?.hide()
           // flProgress?.visibility = View.GONE
        } catch (e:Exception){
           // progress_bar?.hide()
           // flProgress?.visibility = View.GONE
        }

    }

    private fun showBatteryLavel(p0: Int) {
        txtPercentage.text = "$p0 %"
        p0?.let {
            if (p0 > 30) {
                if (p0 < 60) {
                    Glide.with(requireContext())
                        .load(R.drawable.ic_battery_medium)
                        .skipMemoryCache(true)
                        .into(imgBatteryStatus)
                } else {
                    Glide.with(requireContext())
                        .load(R.drawable.ic_battery_full)
                        .skipMemoryCache(true)
                        .into(imgBatteryStatus)
                }
            } else {
                Glide.with(requireContext())
                    .load(R.drawable.ic_battery_low)
                    .skipMemoryCache(true)
                    .into(imgBatteryStatus)
                requireContext().toast("Low Battery,Please Check Lock Battery...")
            }
        }
    }
}

