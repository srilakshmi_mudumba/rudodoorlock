package com.app.rudo.view.lockdetails.iccard

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.contrants.Constants
import com.app.rudo.databinding.FragmentIcTimedBinding
import com.app.rudo.model.fingerprint.FingerprintData
import com.app.rudo.model.iccard.IccardData
import com.app.rudo.model.locklist.Lock
import com.app.rudo.utils.*
import com.ttlock.bl.sdk.api.TTLockClient
import com.ttlock.bl.sdk.callback.AddFingerprintCallback
import com.ttlock.bl.sdk.callback.AddICCardCallback
import com.ttlock.bl.sdk.entity.LockError
import kotlinx.android.synthetic.main.fragment_ic_timed.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 12-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class AddIcTimedFragment(private val lock: Lock, private val type: String) : Fragment(),
    KodeinAware, IccardImpl,
    DialogUitls.OnClickDialogItemImpl,
DateUtils.Companion.DateTimePickerImpl{
    override val kodein: Kodein by kodein()
    private val factory: IccardViewModelFactory by instance<IccardViewModelFactory>()
    private var viewmodel: IcCardViewModel? = null
    private var dialog: AlertDialog? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentIcTimedBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_ic_timed, container
            , false
        )
        viewmodel = ViewModelProvider(this, factory).get(IcCardViewModel::class.java)
        binding.ict = viewmodel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (type.equals("Fingerprint", ignoreCase = true)) {
            (activity as AppCompatActivity).supportActionBar?.title = "Fingerprint"
        }
        viewmodel?.registerListner(this)
        viewmodel?.lock = lock
        val startTime = DateUtils.getCurrentDateTime("startTime")
        val endTime = DateUtils.getCurrentDateTime("endTime")
        stateTime.text = DateUtils.getFormatedDateAndTime(startTime)
        endDate.text = DateUtils.getFormatedDateAndTime(endTime)
        viewmodel?.startDate = startTime
        viewmodel?.endDate = endTime
        stateTime.setOnClickListener{
            DateUtils.getDatePicker(requireContext(),Constants.START_TIME,this)
        }
        endDate.setOnClickListener{
            DateUtils.getDatePicker(requireContext(),Constants.END_TIME,this)
        }
    }

    override fun hideProgress() {
        progress_bar_ict.hide()
    }

    override fun onStated() {
        context?.hideKeyboard(view?.rootView!!)
        progress_bar_ict.show()
    }

    override fun onSuccess(list: List<IccardData>) {
        progress_bar_ict.hide()
    }

    override fun onFailure(message: String) {
        progress_bar_ict.hide()
        view?.rootView?.snackbar(message)
    }

    override fun addCardSuccess(cardId: Int,type: String) {
        edtEkeyName.text = null
        progress_bar_ict.hide()
        dialog = showAlertDialog {
            eText.text = cardId.toString()
            txtMessage.text = type+" id :"
            btnClickListener {
                dialog?.cancel()
            }
        }
        dialog?.setCancelable(false)
        dialog?.show()
    }

    override fun fingerPrintList(list: List<FingerprintData>) {

    }

    override fun onClickDelete(type: String) {

    }

    override fun onTimedError() {
        requireContext().toast(getString(R.string.time_error))
    }

    override fun onClickOkButton() {
        if (type.isNotEmpty() && type == "Fingerprint") {
            TTLockClient.getDefault().addFingerprint(
                viewmodel?.startDate!!,
                viewmodel?.endDate!!,
                lock.lockData,
                lock.lockMac,
                object : AddFingerprintCallback {
                    override fun onEnterAddMode(totalCount: Int) {
                        requireContext().toast("==put your fingerprint on lock=$totalCount")
                    }

                    override fun onCollectFingerprint(currentCount: Int) {
                        requireContext().toast("==currentCount is $currentCount")
                    }

                    override fun onAddFingerpintFinished(fingerprintNum: Long) {
                        viewmodel?.addFingerprint(fingerprintNum.toString())
                    }

                    override fun onFail(error: LockError) {
                        if(error?.name == "LOCK_CONNECT_FAIL"){
                            requireContext().toast(requireContext().getString(R.string.bluetooth_operation_failed))
                        }else {
                            view?.rootView?.snackbar(error.errorMsg)
                        }
                    }
                })
        } else {
            TTLockClient.getDefault().addICCard(
                viewmodel?.startDate!!,
                viewmodel?.endDate!!,
                lock.lockData,
                lock.lockMac,
                object : AddICCardCallback {
                    override fun onEnterAddMode() {
                        requireContext().toast("-you can put ic card on lock now-")
                    }

                    override fun onAddICCardSuccess(cardNum: Long) {
                        // view?.rootView?.snackbar("card is added to lock -$cardNum")
                        viewmodel?.uploadIcCardData(cardNum.toString())
                    }

                    override fun onFail(error: LockError) {
                        if(error?.name == "LOCK_CONNECT_FAIL"){
                            requireContext().toast(requireContext().getString(R.string.bluetooth_operation_failed))
                        }else {
                            view?.rootView?.snackbar(error.errorMsg)
                        }
                    }
                })
        }

    }

    override fun onClickYes(dialog: Dialog) {
        dialog.cancel()
    }

    override fun onClickNo(dialog: Dialog) {

    }

    override fun onClickDialogDelete(password: String) {

    }


    override fun onClickCancel(dialog: Dialog) {

    }

    override fun setDateTime(millsec: Long, identifyer: String) {
        val temp = DateUtils.getFormatedDateAndTime(millsec)
        if(Constants.START_TIME.equals(identifyer,ignoreCase = true)){
            stateTime.text = temp
            viewmodel?.startDate = millsec
        }else if(Constants.END_TIME.equals(identifyer,ignoreCase = true)){
            viewmodel?.endDate = millsec
            endDate.text = temp
        }
    }
}