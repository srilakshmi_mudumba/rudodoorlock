package com.app.rudo.view.account

/*
// Created by Satyabrata Bhuyan on 29-09-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

interface AccountImple {
    fun onError(message : String)
    fun onSuccess()
}