package com.app.rudo.view.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.app.rudo.BuildConfig
import com.app.rudo.R
import com.app.rudo.contrants.Constants
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.view.MainActivity
import com.app.rudo.view.auth.LoginActivity
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability
import kotlinx.android.synthetic.main.activity_splash.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein

class SplashActivity : AppCompatActivity(),KodeinAware {

    override val kodein by kodein()
    private var preferance : AppPrefrences? = null
    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 3000 //3 seconds
    private val MY_REQUEST_CODE = 1
    private val versionName = BuildConfig.VERSION_NAME
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        preferance = AppPrefrences(applicationContext)
        rippleBackground.startRippleAnimation()
        setUpRunnable()
       // appUpdate()
    }
    fun setUpRunnable(){
        //Initialize the Handler
        mDelayHandler = Handler()
        //Navigate with delay
        mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)
    }

    override fun onDestroy() {
        super.onDestroy()
        rippleBackground.stopRippleAnimation()
    }

    internal val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            val accessToken = preferance?.getAccessToken()
            if(accessToken.isNullOrEmpty()) {
                Constants.PINSCREEN = "LOGIN"
                Intent(applicationContext, LoginActivity::class.java).also {
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    applicationContext.startActivity(it)
                    finish()
                }
            }else{
                Constants.PINSCREEN = "HOME"
                Intent(applicationContext, MainActivity::class.java).also {
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    applicationContext.startActivity(it)
                    finish()
                }
               /* Intent(applicationContext, HomeActivity::class.java).also {
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    applicationContext.startActivity(it)
                    finish()
                }*/
            }
        }
    }

    fun appUpdate(){
        // Creates instance of the manager.
        val appUpdateManager = AppUpdateManagerFactory.create(this)

// Returns an intent object that you use to check for an update.
        val appUpdateInfoTask = appUpdateManager.appUpdateInfo

// Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                // For a flexible update, use AppUpdateType.FLEXIBLE
                && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
            ) {
                // Request the update.

                appUpdateManager.startUpdateFlowForResult(
                    // Pass the intent that is returned by 'getAppUpdateInfo()'.
                    appUpdateInfo,
                    // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                    AppUpdateType.IMMEDIATE,
                    // The current activity making the update request.
                    this,
                    // Include a request code to later monitor this update request.
                    MY_REQUEST_CODE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MY_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                //log("Update flow failed! Result code: $resultCode")
                // If the update is cancelled or fails,
                // you can request to start the update again.
            }
        }
    }
}
