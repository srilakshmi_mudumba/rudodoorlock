package com.app.rudo.view.addlock

/*
// Created by Satyabrata Bhuyan on 11-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

interface AddLockImpl {
    fun onStarted()
    fun onSuccess()
    fun onFailure(message:String)
}