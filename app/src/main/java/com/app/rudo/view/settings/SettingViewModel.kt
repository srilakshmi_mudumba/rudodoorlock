package com.app.rudo.view.settings

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.app.rudo.BuildConfig
import com.app.rudo.R
import com.app.rudo.contrants.Constants
import com.app.rudo.model.lockdetails.LockDetails
import com.app.rudo.model.locklist.Lock
import com.app.rudo.repository.AuthRepository
import com.app.rudo.repository.LockDetailRepository
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.utils.NoInternetException
import com.ttlock.bl.sdk.util.DigitUtil
import kotlinx.coroutines.launch

/*
// Created by Satyabrata Bhuyan on 19-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class SettingViewModel(
    private val repository: LockDetailRepository,
    private val authRepository: AuthRepository,
    private val pref: AppPrefrences
) : ViewModel() {
    var listner: SettingsImpl? = null
    var lockDetails: LockDetails? = null
    var lock: Lock? = null
    fun registerListner(impl: SettingsImpl) {
        this.listner = impl
    }

    fun OnClickLogout(view: View) {
        Constants.PINSCREEN = "LOGIN"

        pref.clear()
        listner?.onLogout()
    }

    fun OnClickBasic(view: View) {
        val bundle = Bundle()
        bundle.putSerializable("lockdetails", lockDetails)
        view.findNavController().navigate(R.id.action_lockSettings_to_basic, bundle)
    }

    fun OnClickUnlock(view: View) {
        val bundle = Bundle()
        bundle.putSerializable("lockData", lock)
        view.findNavController().navigate(R.id.fragmentUnlockRemotely, bundle)
    }

    fun OnClickAutoLock(view: View) {
        val bundle = Bundle()
        bundle.putSerializable("lockData", lock)
        view.findNavController().navigate(R.id.fragmentAutoLock, bundle)
    }

    fun OnClickLockSound(view: View) {
        val bundle = Bundle()
        bundle.putSerializable("lockData", lock)
        view.findNavController().navigate(R.id.fragmentLockSound, bundle)
    }

    fun OnClickDelete(view: View) {
        listner?.onDeleteOntion()
    }

    fun OnClickLockDelete(view: View) {
        lockDetails?.let {
            listner?.onDeleteOntion(lockDetails!!)
        }
    }


    fun getLockDeatils(lockId: Int) {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            keyDetailsFromOpenTT(lockId)
        } else {
            keyDetailsFromYutu(lockId)
        }
    }

    private fun keyDetailsFromYutu(lockId: Int) {
        //listner?.onStarted()
        viewModelScope.launch {
            try {
                val result = repository.getLockDetails(lockId)
                result.let {
                    lockDetails = it
                    // lockDeatilsImpl?.onSuccesLockDetails(it)
                }
            } catch (e: ApiException) {
                listner?.onError(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onError(e.message!!)
            }
        }
    }

    private fun keyDetailsFromOpenTT(lockId: Int) {
        viewModelScope.launch {
            try {
                val result = repository.getLockDetailsFromOpenTTApi(lockId)
                result.let {
                    lockDetails = it
                    // lockDeatilsImpl?.onSuccesLockDetails(it)
                }
            } catch (e: ApiException) {
                listner?.onError(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onError(e.message!!)
            }
        }
    }

    fun deleteUser(password: String) {
        viewModelScope.launch {
            val pwd = DigitUtil.getMD5(password.trim())
            val response = authRepository.userLogin(pref.getUserInfo()!!, pwd)
            response.let {
                if (response.errmsg.isNullOrEmpty()) {
                    try {
                        val deleteResponse = repository.deletUser()
                        deleteResponse?.let {
                            if (it.errcode != null && it.errcode == 0) {
                                pref.clear()
                                listner?.onLogout()
                            } else {
                                listner?.onError(it.errmsg!!)
                            }
                        }
                    } catch (apiError: ApiException) {
                        listner?.onError(apiError.message!!)
                    } catch (error: NoInternetException) {
                        listner?.onError(error.message!!)
                    }
                } else {
                    listner?.onError(response.errmsg)
                }
            }
        }

    }

    fun login(password: String){
        listner?.onStarted()
        try {
            val pwd = DigitUtil.getMD5(password.trim())
            viewModelScope.launch {
                val response = authRepository.userLogin(pref.getUserInfo()!!, pwd)
                response.let {
                    if (response.errmsg.isNullOrEmpty()) {
                        listner?.onSuccessLogin()
                    }else{
                        listner?.onError(response.errmsg)
                    }
                }
            }
        }catch (e:Exception){

        }
    }

    fun deleteLock(lockId: Int) {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            deleteLockOpenTT(lockId)
        } else {
            deleteLockYutu(lockId)
        }
    }

    private fun deleteLockOpenTT(lockId: Int) {
        try {
            viewModelScope.launch {
                val response = repository.deleteLockOpenTT(lockId)
                response?.let {
                    if (it.errcode != null && it.errcode == 0) {
                        listner?.onLogout()
                    } else {
                        listner?.onError(it.errmsg!!)
                    }
                }
            }
        } catch (apiError: ApiException) {
            listner?.onError(apiError.message!!)
        } catch (error: NoInternetException) {
            listner?.onError(error.message!!)
        }
    }

    private fun deleteLockYutu(lockId: Int) {
        try {
            viewModelScope.launch {
                val response = repository.deleteLock(lockId)
                response?.let {
                    if (it.errcode != null && it.errcode == 0) {
                        listner?.onLogout()
                    } else {
                        listner?.onError(it.errmsg!!)
                    }
                }
            }
        } catch (apiError: ApiException) {
            listner?.onError(apiError.message!!)
        } catch (error: NoInternetException) {
            listner?.onError(error.message!!)
        }
    }

    fun onClickAbout(view: View) {
        view?.findNavController()?.navigate(R.id.fragmentAbout)
    }
}