package com.app.rudo.view.auth

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.BackgroundColorSpan
import android.text.style.ClickableSpan
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.databinding.ActivitySignupBinding
import com.app.rudo.utils.hide
import com.app.rudo.utils.show
import com.app.rudo.utils.snackbar
import kotlinx.android.synthetic.main.activity_signup.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class SignupActivity : AppCompatActivity(),AuthListnerImpl, KodeinAware {

    override val kodein by kodein()
    private val factory: AuthViewModelFactory by instance<AuthViewModelFactory>()

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

       val  binding: ActivitySignupBinding = DataBindingUtil.setContentView(this, R.layout.activity_signup)
       val  viewModel = ViewModelProvider(this, factory).get(AuthViewModel::class.java)
        binding.viewmodel = viewModel
        viewModel.setListner(this)

        val text = "I agree End User License Agreement (EULA)"
        val spannableString = SpannableString(text)
        val color = getColor(R.color.white)
        val foregroundColorSpanCyan = BackgroundColorSpan(color)
        spannableString.setSpan(foregroundColorSpanCyan, 34, text.length,
            Spanned.SPAN_EXCLUSIVE_INCLUSIVE)

        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                navigateToTncActivity()
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
            }
        }

        spannableString.setSpan(clickableSpan, 34, text.length, 0)
        textTnc.movementMethod = LinkMovementMethod.getInstance()
        textTnc.text = spannableString
/*
        textTnc.setOnClickListener {
            navigateToTncActivity()
        }*/

    }

    override fun onStarted() {
        progress_bar.show()
    }

    override fun onSuccess() {
        progress_bar.hide()
        navigateTo()
    }

    override fun onForgetPasswordSuccess() {

    }

    override fun onFailure(message: String) {
        progress_bar.hide()
        root.snackbar(message)
    }

    override fun onClickForgetPassword() {

    }

    override fun navigateTo() {
        Intent(this, LoginActivity::class.java).also {
            it.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            it.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(it)
            finish()
        }
    }

    fun navigateToTncActivity(){
        Intent(this, TNCActivity::class.java).also {
           // it.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
           // it.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(it)
        }
    }
    override fun onLoginButtonClick() {

    }

    override fun otpGeneratedSuccess() {

    }

    override fun onForgotCancel() {

    }
}
