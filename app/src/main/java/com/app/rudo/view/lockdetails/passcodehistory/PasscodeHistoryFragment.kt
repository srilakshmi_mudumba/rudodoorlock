package com.app.rudo.view.lockdetails.passcodehistory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.rudo.R
import com.app.rudo.model.PassCodeListModel
import com.app.rudo.model.ekeys.EAccessKey
import com.app.rudo.model.lockdetails.KeyData
import com.app.rudo.model.locklist.Lock
import com.app.rudo.model.recodes.RecodeDetailsModel
import com.app.rudo.utils.hide
import com.app.rudo.utils.show
import com.app.rudo.utils.snackbar
import com.app.rudo.utils.toast
import com.app.rudo.view.lockdetails.LockDeatilsImpl
import com.app.rudo.view.lockdetails.LockDetailsViewModel
import com.app.rudo.view.lockdetails.LockDetailsViewmodelFactory
import com.app.rudo.view.lockdetails.iccard.SimpleDividerItemDecoration
import com.app.rudo.view.lockdetails.iccard.SwipeToDeleteCallback
import com.ttlock.bl.sdk.api.TTLockClient
import com.ttlock.bl.sdk.callback.DeletePasscodeCallback
import com.ttlock.bl.sdk.entity.LockError
import kotlinx.android.synthetic.main.fragment_passcodes_history.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 10-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class PasscodeHistoryFragment : Fragment(), LockDeatilsImpl, KodeinAware,
    PasscodeHistoryAdapter.ItemPasscodeClicked {

    override val kodein: Kodein by kodein()
    private val factory: LockDetailsViewmodelFactory by instance<LockDetailsViewmodelFactory>()
    var viewmodel: LockDetailsViewModel? = null
    private var lock:Lock? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_passcodes_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewmodel = ViewModelProvider(this, factory).get(LockDetailsViewModel::class.java)
        viewmodel?.registerListner(this)

        arguments?.let {
            lock = (Lock::class.java).cast(arguments?.getSerializable("lockData"))
            viewmodel?.getPassCodes(lock!!.lockId!!)
        }
        simpleSwipeRefreshLayout.setOnRefreshListener {  viewmodel?.getPassCodes(lock!!.lockId!!) }
        // viewmodel.get
    }

    override fun onProgressHide() {
        progress_bar?.hide()
    }

    override fun onStarted() {
        progress_bar?.show()
    }

    override fun onSuccess() {
        progress_bar?.hide()
    }

    override fun onSuccesLockDetails(lockDetails: KeyData) {
        progress_bar?.hide()
    }

    override fun onFailure(message: String) {
        progress_bar?.hide()
        view?.rootView?.snackbar(message)
    }


    override fun onSuccessLockKey(lockKeyList: List<EAccessKey>) {


    }

    override fun onSuccessPassCodes(lockKeyList: List<PassCodeListModel>) {
        progress_bar?.hide()
        simpleSwipeRefreshLayout?.isRefreshing = false
        recycler_view?.also {
            if(!lockKeyList.isEmpty()) {
                it.layoutManager = LinearLayoutManager(requireContext())
                it.setHasFixedSize(true)
                // val reversedList = lockKeyList.reversed()
                it.adapter =
                    PasscodeHistoryAdapter(lockKeyList as MutableList<PassCodeListModel>, this)
                it.addItemDecoration(SimpleDividerItemDecoration(requireContext()))

                val swipeToDeleteCallback =
                    object : SwipeToDeleteCallback(
                        requireContext(),
                        0,
                        ItemTouchHelper.RIGHT
                    ) {
                        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                            (it.adapter as PasscodeHistoryAdapter).pendingRemoval(viewHolder.adapterPosition)
                        }

                        override fun getSwipeDirs(
                            recyclerView: RecyclerView,
                            viewHolder: RecyclerView.ViewHolder
                        ): Int {
                            if ((it.adapter as PasscodeHistoryAdapter).isPendingRemoval(viewHolder.adapterPosition)) {
                                return ItemTouchHelper.ACTION_STATE_IDLE
                            }
                            return super.getSwipeDirs(recyclerView, viewHolder)
                        }
                    }

                val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
                itemTouchHelper.attachToRecyclerView(it)
            }else{
                it.visibility = View.GONE
                flNoData.visibility = View.VISIBLE
            }
        }
    }

    override fun onSuccessUnlockRecords(list: List<RecodeDetailsModel>) {

    }

    override fun onEAccessKeyDeleteSuccess() {

    }

    override fun onDeletePasscodeFromDetails(passcodeData: PassCodeListModel) {

    }



    override fun deletePasscodeItem(passcodeData: PassCodeListModel) {
        progress_bar?.show()
        TTLockClient.getDefault().deletePasscode(passcodeData.keyboardPwd,
        lock?.lockData,
        lock?.lockMac,
        object :DeletePasscodeCallback{
            override fun onFail(error: LockError?) {
                progress_bar?.hide()
                if (error?.name == "LOCK_CONNECT_FAIL") {
                    requireContext().toast(requireContext().getString(R.string.bluetooth_operation_failed))
                } else{

                }
            }

            override fun onDeletePasscodeSuccess() {
                viewmodel?.deletePasscode(passcodeData)
            }


        })

    }

    override fun itemSelected(passcodeData: PassCodeListModel) {
        val bundle = Bundle()
        bundle.putSerializable("passcodedetails",passcodeData)
        bundle.putSerializable("lockData",lock)
        view?.findNavController()?.navigate(R.id.fragmentPasscodeDetails,bundle)
    }
}