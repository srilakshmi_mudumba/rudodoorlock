package com.app.rudo.view.settings

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.app.rudo.BuildConfig
import com.app.rudo.R
import kotlinx.android.synthetic.main.fragment_about.*

/*
// Created by Satyabrata Bhuyan on 31-08-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class AboutFragment : Fragment() {
    private val versionName = BuildConfig.VERSION_NAME

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =inflater.inflate(R.layout.fragment_about,container,false)
        return view
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtVersion.text ="Version : $versionName"
        llOurStory.setOnClickListener {
            view?.findNavController()?.navigate(R.id.fragmentOurStory)
        }
    }
}