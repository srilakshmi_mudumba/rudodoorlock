package com.app.rudo.view.lockdetails.iccard

import android.app.Dialog
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.rudo.R
import com.app.rudo.databinding.FragmentIcCardBinding
import com.app.rudo.model.fingerprint.FingerprintData
import com.app.rudo.model.iccard.IccardData
import com.app.rudo.model.locklist.Lock
import com.app.rudo.utils.*
import com.ttlock.bl.sdk.api.TTLockClient
import com.ttlock.bl.sdk.callback.*
import com.ttlock.bl.sdk.entity.LockError
import kotlinx.android.synthetic.main.fragment_ic_card.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class IcCardFragment : Fragment(), IccardImpl, KodeinAware,ItemClickListner ,DialogUitls.OnClickDialogItemImpl{

    override val kodein: Kodein by kodein()

    private val factory: IccardViewModelFactory by instance<IccardViewModelFactory>()

    private var viewModel: IcCardViewModel? = null

    private var lock: Lock? = null
    var type:String? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentIcCardBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_ic_card, container, false)
        viewModel = ViewModelProvider(this, factory).get(IcCardViewModel::class.java)
        binding.fmiccard = viewModel
        viewModel?.registerListner(this)
        binding.lifecycleOwner = this
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {

            lock = (Lock::class.java).cast(arguments?.getSerializable("lockData"))
            lock?.let { viewModel?.lock = lock }
            type = arguments?.getString("type")
            type?.let {
                viewModel?.type = type
                if (type.equals("Fingerprint", ignoreCase = true)) {
                    (activity as AppCompatActivity).supportActionBar?.title = "Fingerprint"
                    viewModel?.getFingerprintList()
                } else {
                    viewModel?.getICCardList()
                }
            }
        }
        simpleSwipeRefreshLayout.setOnRefreshListener {
            if (type.equals("Fingerprint", ignoreCase = true)) {
                (activity as AppCompatActivity).supportActionBar?.title = "Fingerprint"
                viewModel?.getFingerprintList()
            } else {
                viewModel?.getICCardList()
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.option_menu, menu)
        menu.findItem(R.id.addGateway).isVisible = false
        if ("Fingerprint" == type) {
            menu.findItem(R.id.addCard).title = "Add Fingerprint"
            menu.findItem(R.id.clearCard).title = "Clear All Fingerprint"
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.addCard -> {

                // view?.findNavController()?.navigate(R.id.action_gatewayacc_to_addgateway)
                val args = Bundle()
                args.putSerializable("lockData", lock)
                args.putString("type", type)
                view?.findNavController()?.navigate(R.id.action_iccard_to_addIccard, args)
                true
            }
            R.id.clearCard ->{
                DialogUitls.showDialog(
                    requireContext(),
                    "Clear All",
                    "Do you want to Clear All?",
                    true,
                    true,
                    false,
                    this
                )
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun hideProgress() {
        progress_bar_ic?.hide()
    }

    override fun onStated() {
        progress_bar_ic?.show()
    }

    override fun onSuccess(list: List<IccardData>) {
        progress_bar_ic?.hide()
        simpleSwipeRefreshLayout?.isRefreshing = false
        recycler_view_iccard?.also {
            if(list.isNotEmpty()) {
                it.layoutManager = LinearLayoutManager(requireContext())
                it.setHasFixedSize(true)
                it.adapter = IccardAdapter(list as MutableList<IccardData>, this)

                recycler_view_iccard.addItemDecoration(SimpleDividerItemDecoration(requireContext()))

                val swipeToDeleteCallback =
                    object : SwipeToDeleteCallback(
                        requireContext(),
                        0,
                        ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
                    ) {
                        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                            (it.adapter as IccardAdapter).pendingRemoval(viewHolder.adapterPosition)
                        }

                        override fun getSwipeDirs(
                            recyclerView: RecyclerView,
                            viewHolder: RecyclerView.ViewHolder
                        ): Int {
                            if ((it.adapter as IccardAdapter).isPendingRemoval(viewHolder.adapterPosition)) {
                                return ItemTouchHelper.ACTION_STATE_IDLE
                            }
                            return super.getSwipeDirs(recyclerView, viewHolder)
                        }
                    }

                val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
                itemTouchHelper.attachToRecyclerView(recycler_view_iccard)
            }else{
                it.visibility = View.GONE
                flNoData?.visibility = View.VISIBLE
            }
        }
    }

    override fun onFailure(message: String) {
        progress_bar_ic.hide()
        view?.rootView?.snackbar(message)
    }

    override fun onClickOkButton() {

    }

    override fun addCardSuccess(cardId: Int, type: String) {

    }

    override fun fingerPrintList(list: List<FingerprintData>) {
        progress_bar_ic?.hide()
        simpleSwipeRefreshLayout?.isRefreshing = false
        recycler_view_iccard?.also {
            if(list.isNotEmpty()) {
                it.layoutManager = LinearLayoutManager(requireContext())
                it.setHasFixedSize(true)
                it.adapter = FingerprintAdapter(list as MutableList<FingerprintData>, this)

                it.addItemDecoration(SimpleDividerItemDecoration(requireContext()))

                val swipeToDeleteCallback =
                    object : SwipeToDeleteCallback(
                        requireContext(),
                        0,
                        ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
                    ) {
                        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                            (it.adapter as FingerprintAdapter).pendingRemoval(viewHolder.adapterPosition)
                        }

                        override fun getSwipeDirs(
                            recyclerView: RecyclerView,
                            viewHolder: RecyclerView.ViewHolder
                        ): Int {
                            if ((it.adapter as FingerprintAdapter).isPendingRemoval(viewHolder.adapterPosition)) {
                                return ItemTouchHelper.ACTION_STATE_IDLE
                            }
                            return super.getSwipeDirs(recyclerView, viewHolder)
                        }
                    }

                val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
                itemTouchHelper.attachToRecyclerView(recycler_view_iccard)
            }else{
                it.visibility = View.GONE
                flNoData?.visibility = View.VISIBLE
            }
        }
    }

    override fun onClickDelete(type: String) {
        if ("Fingerprint" == type) {
            clearAllFingerprint()
        } else {
            clearAllCards()
        }
    }

    override fun onTimedError() {
        requireContext().toast(getString(R.string.time_error))
    }

    private fun clearAllCards() {
        progress_bar_ic?.show()
        try {

            TTLockClient.getDefault().clearAllICCard(
                lock?.lockData,
                lock?.lockMac,
                object : ClearAllICCardCallback {
                    override fun onClearAllICCardSuccess() {
//                makeToast("--all ic cards have been cleared success-");
                        //this must be done
                        //uploadClear2Server()
                        viewModel?.clearIcCard()
                    }

                    override fun onFail(error: LockError) {
                        progress_bar_ic?.hide()
                        requireContext()?.toast(requireContext().getString(R.string.bluetooth_operation_failed))
                    }
                })
        }catch (e:Exception){

        }
    }
    private fun deleteCard(iccardData: IccardData){
        progress_bar_ic?.show()
        try {


            TTLockClient.getDefault().deleteICCard(
                iccardData.cardNumber,
                lock?.lockData,
                lock?.lockMac,
                object : DeleteICCardCallback {
                    override fun onDeleteICCardSuccess() {
                        viewModel?.deleteIcCard(iccardData)
                    }

                    override fun onFail(p0: LockError?) {
                        progress_bar_ic?.hide()
                        requireContext()?.toast(requireContext().getString(R.string.bluetooth_operation_failed))
                    }

                }
            )
        }catch (e:Exception){

        }
    }

    private fun clearAllFingerprint() {
        progress_bar_ic?.show()
        try {
            TTLockClient.getDefault().clearAllFingerprints(
                lock?.lockData,
                lock?.lockMac,
                object : ClearAllFingerprintCallback {
                    override fun onClearAllFingerprintSuccess() {
//                makeToast("--all ic cards have been cleared success-");
                        //this must be done
                        //uploadClear2Server()
                        viewModel?.clearFingerprint()
                    }

                    override fun onFail(error: LockError) {
                        progress_bar_ic?.hide()
                        requireContext()?.toast(requireContext().getString(R.string.bluetooth_operation_failed))
                    }
                })
        }catch (e:Exception){

        }
    }
    private fun deleteFingerprint(fingerprintData: FingerprintData) {
        progress_bar_ic?.show()
        try {
            TTLockClient.getDefault().deleteFingerprint(
                fingerprintData.fingerprintNumber,
                lock?.lockData,
                lock?.lockMac,
                object : DeleteFingerprintCallback {
                    override fun onFail(error: LockError?) {
                        progress_bar_ic?.hide()
                        requireContext()?.toast(requireContext().getString(R.string.bluetooth_operation_failed))
                    }

                    override fun onDeleteFingerprintSuccess() {
                        viewModel?.deleteFingerprint(fingerprintData)
                    }

                })
        }catch (e:Exception){

        }

    }
    private fun getAllCards() {

        TTLockClient.getDefault().getAllValidICCards(
            lock?.lockData,
            lock?.lockMac,
            object : GetAllValidICCardCallback {
                override fun onGetAllValidICCardSuccess(cardDataStr: String) {
                    // makeToast("-all ic cards info $cardDataStr")
                }

                override fun onFail(error: LockError) {
                    // makeErrorToast(error)
                }
            })
    }

    override fun deleteOnClick(iccardData: IccardData) {
        deleteCard(iccardData)
    }

    override fun deleteFingerprintOnClick(fingerprintData: FingerprintData) {
        deleteFingerprint(fingerprintData)
    }

    override fun onClickYes(dialog: Dialog) {
        progress_bar_ic?.show()
        if ("Fingerprint" == type) {
            clearAllFingerprint()
        } else {
            clearAllCards()
        }
    }

    override fun onClickNo(dialog: Dialog) {
        dialog?.dismiss()
    }

    override fun onClickDialogDelete(password: String) {

    }


    override fun onClickCancel(dialog: Dialog) {

    }


}