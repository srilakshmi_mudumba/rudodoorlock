package com.app.rudo.view.customersupport

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import kotlinx.android.synthetic.main.fragment_faq_details.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 26-08-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class FAQDetailsFragment : Fragment(), KodeinAware {

    override val kodein: Kodein by kodein()
    private val factory:SupportViewModelFactory by instance<SupportViewModelFactory>()
    private lateinit var viewModel: SupportViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_faq_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this,factory).get(SupportViewModel::class.java)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val type = arguments?.getString("type")
        val childPosition = arguments?.getInt("childposition")

       // listData["Locks"] = locks
        //listData["Keypad"] = keypad
        //listData["Passcode"] = passcode
        //listData["Mobile App"] = mobileapp
        //listData["Others"] = others
      when(type){
          "Mobile App" ->{
              header.text = "Mobile App"
              if(childPosition == 0){
                  content.text = getString(R.string.mobileapp_zero)
              }else if(childPosition == 1){
                  content.text = getString(R.string.mobileap_first)
              }
             return
          }
          "Keypad" -> {
              header.text = "Keypad"
              if(childPosition == 0){
                  content.text = getString(R.string.keypad_activating)
              }else if(childPosition == 1){
                  content.text = getString(R.string.keypad_flashing)
              } else if(childPosition == 2){
                  content.text = getString(R.string.keypad_avcivated)
              }
              return
          }
          "Passcode" -> {
              header.text = "Passcode"
              if(childPosition == 0){
                  content.text = getString(R.string.passcode_valid)
              }else if(childPosition == 1){
                  content.text = getString(R.string.passcode_first)
              } else if(childPosition == 2){
                  content.text = getString(R.string.passcode_second)
              }
              return
          }
          "Locks" -> {
              header.text = "Locks"
              if(childPosition == 0){
                  content.text = getString(R.string.intoduction)
              }else if(childPosition == 1){
                  content.text = getString(R.string.lock_smart_lock)
              } else if(childPosition == 2){
                  content.text = getString(R.string.lock_methods)
              } else if(childPosition == 3){
                  content.text = getString(R.string.lock_adjust)
              }
              return
          }
          "Others" -> {
              header.text = "Others"
              if(childPosition == 0){
                  content.text = getString(R.string.others_zero)
              }else if(childPosition == 1){
                  content.text = getString(R.string.others_first)
              }
              return
          }
      }
    }
}