package com.app.rudo.view.home.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.rudo.R
import com.app.rudo.contrants.Constants
import com.app.rudo.databinding.FragmentHomeBinding
import com.app.rudo.model.eaccess.EAccessKeyList
import com.app.rudo.utils.hide
import com.app.rudo.utils.show
import com.app.rudo.utils.snackbar
import kotlinx.android.synthetic.main.fragment_home.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class HomeFragment : Fragment(), HomeListnerImpl, LockItemClickImpl, KodeinAware {
    override val kodein by kodein()
    private val factory: HomeViewmodelFactory by instance<HomeViewmodelFactory>()
    private lateinit var viewmodel: HomeViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentHomeBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        viewmodel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
        binding.viewmodel = viewmodel
        viewmodel.registerListner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewmodel.getLockList()
      //  viewmodel.getEKeyList()
        progress_bar?.show()
       /* viewmodel.lockList.observe(viewLifecycleOwner, Observer { lockList ->
            lockList?.let {
                progress_bar?.hide()
                if(!it.isEmpty()) {
                    if (it.size == 1) {
                        Constants.LOCK_COUNT = 1
                        //val bundle = bundleOf("lock" to it[0].lockId)
                        val args = Bundle()
                        args.putSerializable("lockData", it[0])
                        view.findNavController()
                            .navigate(R.id.action_homeFragment_to_lockDetailsFragment, args)
                    } else {
                        recycler_view.also {
                            it.layoutManager = LinearLayoutManager(requireContext())
                            it.setHasFixedSize(true)
                            it.adapter = LockListAdpter(lockList, this)
                        }
                    }
                }Main
            }
        })*/
        viewmodel.lockKeyList.observe(viewLifecycleOwner, Observer { lockList ->
            simpleSwipeRefreshLayout.isRefreshing = false
                progress_bar?.hide()
                if(lockList.isNotEmpty()) {
                    if (lockList.size == 1) {
                        Constants.LOCK_COUNT = 1
                        //val bundle = bundleOf("lock" to it[0].lockId)
                        val args = Bundle()
                        args.putSerializable("lockData", lockList[0])
                        view.findNavController()
                            .navigate(R.id.action_homeFragment_to_lockDetailsFragment, args)
                    } else {
                        recycler_view.also {
                            it.layoutManager = LinearLayoutManager(requireContext())
                            it.setHasFixedSize(true)
                            it.adapter = LockListAdpter(lockList, this)
                        }
                    }
                }else{
                    recycler_view.visibility = View.GONE
                    flPlue.visibility = View.VISIBLE
                }

        })

        imgPluse.setOnClickListener {
            view?.findNavController()?.navigate(R.id.addLockFragment)
        }

        simpleSwipeRefreshLayout.setOnRefreshListener {
            viewmodel.getLockList()
        }

    }


    override fun onStarted() {
        progress_bar?.show()
    }

    override fun onSuccess() {
        progress_bar?.hide()
    }

    override fun onFailure(message: String) {
        progress_bar?.hide()
        view?.rootView?.snackbar(message)
    }

    override fun itemClicked(view: View, lock: EAccessKeyList) {
        val args = Bundle()
        args.putSerializable("lockData", lock)
        view.findNavController().navigate(R.id.action_homeFragment_to_lockDetailsFragment, args)
    }


}