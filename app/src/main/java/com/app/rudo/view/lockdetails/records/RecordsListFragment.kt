package com.app.rudo.view.lockdetails.records

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.rudo.R
import com.app.rudo.model.PassCodeListModel
import com.app.rudo.model.ekeys.EAccessKey
import com.app.rudo.model.lockdetails.KeyData
import com.app.rudo.model.recodes.RecodeDetailsModel
import com.app.rudo.utils.hide
import com.app.rudo.utils.show
import com.app.rudo.utils.snackbar
import com.app.rudo.view.lockdetails.LockDeatilsImpl
import com.app.rudo.view.lockdetails.LockDetailsViewModel
import com.app.rudo.view.lockdetails.LockDetailsViewmodelFactory
import kotlinx.android.synthetic.main.fragment_unlock_recodes.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 10-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class RecordsListFragment : Fragment(), LockDeatilsImpl, KodeinAware {

    override val kodein: Kodein by kodein()
    private val factory: LockDetailsViewmodelFactory by instance<LockDetailsViewmodelFactory>()
    private var lockId: Int? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_unlock_recodes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewmodel = ViewModelProvider(this, factory).get(LockDetailsViewModel::class.java)
        viewmodel.registerListner(this)
        arguments?.let {
            lockId = it.getInt("lock")
            viewmodel.setSelectedLockId(lockId!!)
            viewmodel.getUnlockRecords()
        }
        simpleSwipeRefreshLayout.setOnRefreshListener {
            viewmodel.getUnlockRecords()
        }
    }

    override fun onProgressHide() {
        progress_bar?.hide()
    }

    override fun onStarted() {
        progress_bar?.show()
    }

    override fun onSuccess() {
        progress_bar?.hide()
    }

    override fun onSuccesLockDetails(lockDetails: KeyData) {
        progress_bar.hide()
    }

    override fun onFailure(message: String) {
        progress_bar?.hide()
        view?.rootView?.snackbar(message)
    }


    override fun onSuccessLockKey(lockKeyList: List<EAccessKey>) {


    }

    override fun onSuccessPassCodes(lockKeyList: List<PassCodeListModel>) {

    }

    override fun onSuccessUnlockRecords(list: List<RecodeDetailsModel>) {
        progress_bar.hide()
        simpleSwipeRefreshLayout.isRefreshing = false
        recycler_view.also {
            if (list.isNotEmpty()) {
                it.layoutManager = LinearLayoutManager(requireContext())
                // it.setHasFixedSize(true)
                it.adapter = RecordsListAdapter(list)
            } else {
                it.visibility = View.GONE
                flNoData.visibility = View.VISIBLE
            }
        }
    }

    override fun onEAccessKeyDeleteSuccess() {

    }

    override fun onDeletePasscodeFromDetails(passcodeData: PassCodeListModel) {

    }
}