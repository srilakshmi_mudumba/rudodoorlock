package com.app.rudo.view.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.app.rudo.R
import com.app.rudo.databinding.FragmentResetPasswordBinding
import com.app.rudo.utils.hideKeyboard
import com.app.rudo.utils.snackbar
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 29-09-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class ResetPassword: Fragment() , KodeinAware,AccountImple {
    override val kodein: Kodein by kodein()
    private val factory: AccountViewModelFactory by instance<AccountViewModelFactory>()
    private var viewModel:AccountViewModel? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentResetPasswordBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_reset_password, container, false)
        viewModel = ViewModelProvider(this, factory).get(AccountViewModel::class.java)
        binding.restVm = viewModel
        viewModel?.registerListner(this)
        return binding.root
    }

    override fun onError(message: String) {
        context?.hideKeyboard(view?.rootView!!)
        view?.snackbar(message)
    }

    override fun onSuccess() {
        context?.hideKeyboard(view?.rootView!!)
        view?.findNavController()?.popBackStack()
    }
}