package com.app.rudo.view.settings

import com.app.rudo.model.lockdetails.LockDetails

/*
// Created by Satyabrata Bhuyan on 20-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

interface SettingsImpl {
    fun onStarted()
    fun onLogout()
    fun onDeleteOntion()
    fun onDeleteOntion(lockDetails: LockDetails)
    fun onError(message:String)
    fun onSuccessLogin()
}