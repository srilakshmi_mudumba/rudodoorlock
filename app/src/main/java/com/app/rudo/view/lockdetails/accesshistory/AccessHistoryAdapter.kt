package com.app.rudo.view.lockdetails.accesshistory

import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.rudo.R
import com.app.rudo.databinding.AccessHistoryItemBinding
import com.app.rudo.model.ekeys.EAccessKey

/*
// Created by Satyabrata Bhuyan on 09-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class AccessHistoryAdapter(
    val listData : MutableList<EAccessKey>?,
    val listner : ItemClicked
): RecyclerView.Adapter<AccessHistoryAdapter.AccessHolder>(){
    private var itemsPendingRemoval: MutableList<EAccessKey>? = mutableListOf()
    private var PENDING_REMOVAL_TIMEOUT: Long = 3000
    var handler: Handler? = Handler()
    var pendingRunnables: HashMap<EAccessKey, Runnable>? = HashMap()
    init {
        itemsPendingRemoval = mutableListOf<EAccessKey>()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)=AccessHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.access_history_item,
            parent,
            false
        )
    )

    override fun getItemCount() = listData?.size!!

    override fun onBindViewHolder(holder: AccessHolder, position: Int) {
        holder.accessHistoryItemBinding.keymodel = (listData?.get(position) ?: 0) as EAccessKey?

        if (itemsPendingRemoval!!.contains(listData?.get(position))) {
            //show swipe layout
            holder.accessHistoryItemBinding.swipeLayout.visibility = View.VISIBLE
            holder.accessHistoryItemBinding.itemRoot.visibility = View.GONE

            holder.accessHistoryItemBinding.txtUndo.setOnClickListener {
                listData?.get(position)?.let { it1 -> undoOpt(it1) }
            }
            holder.accessHistoryItemBinding.txtDelete.setOnClickListener {
                listData?.get(position)?.let { it1 -> listner?.deleteItem(it1) }
                // remove(position)
            }

        } else {
            //show regular layout
            holder.accessHistoryItemBinding.swipeLayout.visibility = View.GONE
            holder.accessHistoryItemBinding.itemRoot.visibility = View.VISIBLE

            /* itemView.txt.text = model.name
             itemView.sub_txt.text = model.version
             val id = context.resources.getIdentifier(model.name.toLowerCase(), "drawable", context.packageName)
             itemView.img.setBackgroundResource(id)*/
        }
        holder.accessHistoryItemBinding.root.setOnClickListener{
            listner?.selectedItem(listData?.get(position)!!)
        }
    }

    inner class AccessHolder(
        val accessHistoryItemBinding: AccessHistoryItemBinding
    ):RecyclerView.ViewHolder(accessHistoryItemBinding.root)

    fun undoOpt(model: EAccessKey) {
        val pendingRemovalRunnable: Runnable? = pendingRunnables?.get(model)
        pendingRunnables?.remove(model)
        if (pendingRemovalRunnable != null)
            handler?.removeCallbacks(pendingRemovalRunnable)
        itemsPendingRemoval?.remove(model)
        // this will rebind the row in "normal" state
        listData?.indexOf(model)?.let { notifyItemChanged(it) }
    }

    fun pendingRemoval(position: Int) {

        val data = listData?.get(position)
        if (!itemsPendingRemoval!!.contains(data)) {
            if (data != null) {
                itemsPendingRemoval?.add(data)
            }
            // this will redraw row in "undo" state
            notifyItemChanged(position)
            // let's create, store and post a runnable to remove the data
            /* val pendingRemovalRunnable = Runnable {
                 remove(listData.indexOf(data))
             }

             handler?.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT)
             // pendingRunnables!![data] = pendingRemovalRunnable
             pendingRunnables?.put(data, pendingRemovalRunnable)*/
        }
    }

    fun remove(position: Int) {
        val data = listData?.get(position)
        if (itemsPendingRemoval!!.contains(data)) {
            itemsPendingRemoval?.remove(data)
        }
        if (listData != null) {
            if (listData.contains(data)) {
                //dataList.remove(position)
                listData.removeAt(position)
                notifyItemRemoved(position)
            }
        }
    }

    fun isPendingRemoval(position: Int): Boolean {
        val data = listData?.get(position)
        return itemsPendingRemoval!!.contains(data)
    }

    interface ItemClicked{
        fun deleteItem(eAccessKey: EAccessKey)
        fun selectedItem(eAccessKey: EAccessKey)
    }
}