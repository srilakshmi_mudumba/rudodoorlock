package com.app.rudo.view

import android.annotation.SuppressLint
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.hardware.fingerprint.FingerprintManager
import android.os.Bundle
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.andrognito.pinlockview.PinLockListener
import com.app.rudo.BuildConfig
import com.app.rudo.R
import com.app.rudo.contrants.Constants
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.utils.snackbar
import com.app.rudo.utils.toast
import com.app.rudo.view.home.HomeActivity
import kotlinx.android.synthetic.main.activity_app_auth.*
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey

/*
// Created by Satyabrata Bhuyan on 03-08-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class MainActivity : AppCompatActivity(), AppAuthImpl {

    private var fingerprintManager: FingerprintManager? = null
    private var keyStore: KeyStore? = null
    private var keyGenerator: KeyGenerator? = null
    private val KEY_NAME = "my_key"
    private var cryptoObject: FingerprintManager.CryptoObject? = null
    private val versionName = BuildConfig.VERSION_NAME

    // private var mPinLockView: PinLockView? = null
    // private var mIndicatorDots: IndicatorDots? = null
    private var preferance: AppPrefrences? = null
    private var isPin = false
    private var tempPin: String? = null

    private val mPinLockListener: PinLockListener = object : PinLockListener {
        override fun onComplete(pin: String) {
            if (Constants.PINSCREEN == "HOME") {
                preferance?.let {
                    if (preferance?.getPin() != "") {
                        pin?.let {
                            if (preferance?.getPin() == pin) {
                                onSuccess()
                            } else {
                                pin_lock_view?.resetPinLockView()
                                root?.snackbar("Invalid Pin")
                            }
                        }
                    } else {
                        if (isPin) {
                            if (tempPin == pin) {
                                preferance?.setPin(pin)
                                onSuccess()
                            } else {
                                pin_lock_view?.resetPinLockView()
                                root?.snackbar("Confirm Pin is not matched")
                            }
                        } else {
                            tempPin = pin
                            isPin = true
                            pin_lock_view?.resetPinLockView()
                            profile_name?.text = "Confirm 4 digit Pincode"
                        }
                    }
                }
            } else if (Constants.PINSCREEN == "LOGIN") {
                if (isPin) {
                    if (tempPin == pin) {
                        preferance?.setPin(pin)
                        onSuccess()
                    } else {
                        pin_lock_view?.resetPinLockView()
                        root?.snackbar("Confirm Pin is not matched")
                    }
                } else {
                    tempPin = pin
                    isPin = true
                    pin_lock_view?.resetPinLockView()
                    profile_name?.text = "Confirm 4 digit Pincode"
                }
            }
        }

        override fun onEmpty() {

        }

        override fun onPinChange(pinLength: Int, intermediatePin: String) {

        }
    }

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app_auth)
        preferance = AppPrefrences(applicationContext)
        if (Constants.PINSCREEN == "HOME") {
            overlay?.visibility = View.GONE
            rlroot.visibility = View.VISIBLE
            if (preferance?.getPin() == "") {
                profile_name?.text = "Create 4 digit Pincode"
            } else {
                profile_name?.text = "Enter 4 digit Pincode"
            }

        } else {
            txtForgetPin?.visibility = View.GONE
        }
        /* if(Constants.PINSCREEN == "LOGIN"){
             val intent = Intent(this@MainActivity, CustomPinActivity::class.java)
             intent.putExtra(AppLock.EXTRA_TYPE, AppLock.ENABLE_PINLOCK)
             startActivityForResult(intent, REQUEST_CODE_ENABLE)
         }else{
             val intent = Intent(this@MainActivity, CustomPinActivity::class.java)
             intent.putExtra(AppLock.EXTRA_TYPE, AppLock.UNLOCK_PIN)
             startActivityForResult(intent, REQUEST_CODE_ENABLE)
         }*/
        //mPinLockView = findViewById(R.id.pin_lock_view)
        // mIndicatorDots = findViewById(R.id.indicator_dots)


        //mPinLockView.setCustomKeySet(new int[]{2, 3, 1, 5, 9, 6, 7, 0, 8, 4});
        //mPinLockView.enableLayoutShuffling();

        pin_lock_view?.pinLength = 4
        pin_lock_view?.textColor = ContextCompat.getColor(this, R.color.white)

        // indicator_dots?.indicatorType = IndicatorDots.IndicatorType.FILL
        pin_lock_view?.attachIndicatorDots(indicator_dots)
        pin_lock_view?.setPinLockListener(mPinLockListener)

        version.text = versionName
        if (checkFinger()) {
            fingerprint_iv.visibility = View.VISIBLE
            generateKey()
            val cipher = generateCipher()
            cipher.let {
                cryptoObject = FingerprintManager.CryptoObject(it)
                val helper = FingerprintHelper(this, this)
                helper.startAuth(fingerprintManager!!, cryptoObject!!)
            }
        }else{
            fingerprint_iv.visibility = View.GONE
            toast("FingerPrint is not Register")
        }

        txtForgetPin?.setOnClickListener {
            pin_lock_view?.resetPinLockView()
            profile_name?.text = "Create 4 digit Pincode"
            isPin = false
            preferance?.setPin("")
        }
        btnOk.setOnClickListener {
            rlroot.visibility = View.VISIBLE
            overlay.visibility = View.GONE
        }

    }

    override fun onSuccess() {
        Intent(applicationContext, HomeActivity::class.java).also {
            it.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            applicationContext.startActivity(it)
            finish()
        }
    }

    override fun onFailure() {

    }

    private fun checkFinger(): Boolean {
        // Keyguard Manager
        val keyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
        // Fingerprint Manager
        fingerprintManager = getSystemService(Context.FINGERPRINT_SERVICE) as FingerprintManager
        try {
            // Check if the fingerprint sensor is present
            if (!fingerprintManager!!.isHardwareDetected) {
                // Update the UI with a message
                return false
            }
            if (!fingerprintManager!!.hasEnrolledFingerprints()) {
                return false
            }
            if (!keyguardManager.isKeyguardSecure) {
                return false
            }
        } catch (se: SecurityException) {
            se.printStackTrace()
        }

        return true
    }

    private fun generateKey() {
        try {
            // Get the reference to the key store
            keyStore = KeyStore.getInstance("AndroidKeyStore")
            // Key generator to generate the key
            keyGenerator = KeyGenerator.getInstance(
                KeyProperties.KEY_ALGORITHM_AES,
                "AndroidKeyStore"
            )
            keyStore?.load(null)
            keyGenerator?.init(
                KeyGenParameterSpec.Builder(
                    KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
                )
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                        KeyProperties.ENCRYPTION_PADDING_PKCS7
                    )
                    .build()
            )
            keyGenerator?.generateKey()

        } catch (e: Exception) {

        }
    }


    private fun generateCipher(): Cipher {

        val cipher = Cipher.getInstance(
            KeyProperties.KEY_ALGORITHM_AES + "/"
                    + KeyProperties.BLOCK_MODE_CBC + "/"
                    + KeyProperties.ENCRYPTION_PADDING_PKCS7
        )
        try {
            val key = keyStore?.getKey(
                KEY_NAME,
                null
            ) as SecretKey
            cipher.init(Cipher.ENCRYPT_MODE, key)
        }catch (e:RuntimeException){

        }catch (e:Exception){

        }

        return cipher
    }
}