package com.app.rudo.view.lockdetails.access

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.app.rudo.R
import com.app.rudo.contrants.Constants
import com.app.rudo.databinding.FragmentGivePermanetBinding
import com.app.rudo.model.locklist.Lock
import com.app.rudo.utils.*
import kotlinx.android.synthetic.main.fragment_give_permanet.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class GivePermanetFragment(private var lock: Lock) : Fragment(), GiveAccessImpl, KodeinAware,
    DialogUitls.OnClickDialogItemImpl {
    override val kodein: Kodein by kodein()
    private val factory: GiveAccessViewModelFactory by instance<GiveAccessViewModelFactory>()
    private var viewModel: GiveAccessViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentGivePermanetBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_give_permanet, container, false)
        viewModel = ViewModelProvider(this, factory).get(GiveAccessViewModel::class.java)
        binding.vm = viewModel
        viewModel?.registerListner(this)
        viewModel?.lockId = lock.lockId
        viewModel?.keyId = lock.keyId
        viewModel?.startTime = 0L
        viewModel?.endTime = 0L
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        authorizedAdmin.setOnCheckedChangeListener { buttonView, isChecked ->
            viewModel?.authorizedAdmin = isChecked
        }
        if (lock?.userType != null && Constants.COMMON_KEY == lock.userType) {
            authAdminLinerlayout.visibility = View.GONE
            authViewLine.visibility = View.GONE
        }
    }

    override fun onStarted() {
        context?.hideKeyboard(view?.rootView!!)
        progress_bar.show()

    }

    override fun onSuccess() {
        progress_bar.hide()
        edtRecipients.setText("")
        edtEkeyName.setText("")
        DialogUitls.showDialog(
            requireContext(),
            "",
            "eAccess shared successfully",
            true,
            false,
            true,
            this
        )

    }

    override fun onFailure(message: String) {
        progress_bar.hide()
        view?.rootView?.snackbar(message)
    }

    override fun onTimeError() {
        requireContext().toast(getString(R.string.time_error))
    }

    override fun onClickYes(dialog: Dialog) {
        dialog?.dismiss()
        view?.findNavController()?.popBackStack()
    }

    override fun onClickNo(dialog: Dialog) {

    }

    override fun onClickDialogDelete(password: String) {

    }


    override fun onClickCancel(dialog: Dialog) {

    }
}