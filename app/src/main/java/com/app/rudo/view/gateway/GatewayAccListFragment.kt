package com.app.rudo.view.gateway

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.rudo.R
import com.app.rudo.databinding.FragmentGatewayAccListBinding
import com.app.rudo.model.gateway.AccList
import com.app.rudo.utils.hide
import com.app.rudo.utils.show
import com.app.rudo.utils.snackbar
import kotlinx.android.synthetic.main.fragment_gateway_acc_list.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 23-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class GatewayAccListFragment : Fragment(), KodeinAware, GatewayImpl {
    override val kodein: Kodein by kodein()
    private val REQUEST_PERMISSION_REQ_CODE = 11
    private val factory: GatewayViewmodelFactory by instance<GatewayViewmodelFactory>()
    private var mAdapter: GatewayAccAdapter? = null
    private var viewModel: GatewayViewmodel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: FragmentGatewayAccListBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_gateway_acc_list, container, false)
        viewModel = ViewModelProvider(this, factory).get(GatewayViewmodel::class.java)
        binding.gateway = viewModel
        setHasOptionsMenu(true)
        return binding.root
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.option_menu,menu)
        menu.findItem(R.id.clearCard).isVisible = false
        menu.findItem(R.id.addCard).isVisible = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.addGateway ->{
                view?.findNavController()?.navigate(R.id.action_gatewayacc_to_addgateway)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }


    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel?.getAccList()
    }

    override fun onStarted() {
        progress_bar.show()
    }

    override fun onFailure(message: String) {
        progress_bar.hide()
        view?.rootView?.snackbar(message)
    }

    override fun OnGatewayAccItem(list: List<AccList>) {
        progress_bar.hide()
        recycler_view.also {
            it.layoutManager = LinearLayoutManager(requireContext())
            it.setHasFixedSize(true)
            mAdapter = GatewayAccAdapter(list)
            // mAdapter?.setOnLockItemClick(this)
            it.adapter = mAdapter

        }
    }
}