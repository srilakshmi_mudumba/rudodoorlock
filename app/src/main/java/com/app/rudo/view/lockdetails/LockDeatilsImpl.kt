package com.app.rudo.view.lockdetails

import com.app.rudo.model.PassCodeListModel
import com.app.rudo.model.ekeys.EAccessKey
import com.app.rudo.model.lockdetails.KeyData
import com.app.rudo.model.recodes.RecodeDetailsModel

/*
// Created by Satyabrata Bhuyan on 05-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

interface LockDeatilsImpl {
    fun onProgressHide()
    fun onStarted()
    fun onSuccess()
    fun onSuccesLockDetails(lockDetails: KeyData)
    fun onFailure(message: String)
    fun onSuccessLockKey(lockKeyList:List<EAccessKey>)
    fun onSuccessPassCodes(lockKeyList:List<PassCodeListModel>)
    fun onSuccessUnlockRecords(list:List<RecodeDetailsModel>)
    fun onEAccessKeyDeleteSuccess()
    fun onDeletePasscodeFromDetails(passcodeData: PassCodeListModel)
}