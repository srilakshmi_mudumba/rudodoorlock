package com.app.rudo.view.auth

/*
// Created by Satyabrata Bhuyan on 30-06-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

interface AuthListnerImpl {
    fun onStarted()
    fun onSuccess()
    fun onForgetPasswordSuccess()
    fun onFailure(message:String)
    fun onClickForgetPassword()
    fun navigateTo()
    fun onLoginButtonClick()
    fun otpGeneratedSuccess()
    fun onForgotCancel()
}