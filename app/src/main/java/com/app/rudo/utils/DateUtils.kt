package com.app.rudo.utils

import android.R
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import kotlin.time.ExperimentalTime

/*
// Created by Satyabrata Bhuyan on 09-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class DateUtils {
    @Suppress("DEPRECATION")
    companion object {
        @RequiresApi(Build.VERSION_CODES.O)
        fun getDateAndTime(dateTime: Long): String {
            try {
                val sp = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
                val date = Date(dateTime)
                val t = sp.format(date)
                return t.toString()
            } catch (e: Exception) {

            }
            return ""
        }

        fun getCurrentDateTime(endTime: String): Long {
            val c = Calendar.getInstance()
            if ("endTime" == endTime) {
                c.add(Calendar.HOUR, 1)
                c.add(Calendar.MINUTE, 0)
                c.add(Calendar.SECOND, 0)
            }
            return c.time.time
        }

        fun getFormatedDate(c: Long): String {
            val date = Date(c)
            val sp = SimpleDateFormat("YYYY-MM-dd HH:MM:SS", Locale.getDefault())
            return sp.format(date).toString()
        }

        fun getFormatedDateAndTime(c: Long): String {
           // val calendar = Calendar.getInstance(Locale.getDefault())
             val date =Date(c)
            val pattern = "yyyy-MM-dd HH:mm"
            val simpleDateFormat = SimpleDateFormat(pattern)
            val dateNew = simpleDateFormat.format(date)
            return dateNew.toString()
        }

        @ExperimentalTime
        fun getSeconds(timeLong: Long): String {
            val date = Date(timeLong)
            val sp = SimpleDateFormat("S", Locale.getDefault())
            return sp.format(date).toString() + "s"
        }

        fun getTimeForPasscodeFormart(): Long {
            var result = 0L
            try {
                val c = Calendar.getInstance()
                c.add(Calendar.HOUR, 0)
                return c.time.time
            } catch (e: Exception) {
                Timber.e("Error message", e.message)
            }
            return result
        }

       /* fun getTimePicker(textView: TextView, context: Context) {
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR, hour)
                cal.set(Calendar.MINUTE, minute)

                val temp = getFormatedDateAndTime(cal.time.time)
                textView.text = temp

            }
            TimePickerDialog(
                context,
                R.style.Theme_Holo_Light_Dialog,
                timeSetListener,
                cal.get(Calendar.HOUR),
                cal.get(Calendar.MINUTE),
                true
            ).show()
        }*/
        @SuppressLint("SetTextI18n")
        private fun getTimePicker(cal :Calendar, context: Context,identifyer:String,listner:DateTimePickerImpl) {
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)

               // val temp = getFormatedDateAndTime(cal.time.time)
                listner?.setDateTime(cal.time.time,identifyer)
              //  textView.text = temp

            }
           //val yesDate=System.currentTimeMillis() - (1000 * 60 * 60 * 24)
            val timePicker =TimePickerDialog(
                context,
                R.style.Theme_Holo_Light_Dialog,
                timeSetListener,
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                false
            )
           timePicker.show()
        }
        fun getDatePicker(context: Context,identifyer:String,listner:DateTimePickerImpl) {
            val cal = Calendar.getInstance()
            val dateSetListener =
                DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                    cal.set(Calendar.YEAR, year)
                    cal.set(Calendar.MONTH, month)
                    cal.set(Calendar.DAY_OF_MONTH,dayOfMonth)
                    //val temp = getFormatedDateAndTime(cal.timeInMillis)
                    getTimePicker(cal,context,identifyer,listner)
                }/*{ da, hour, minute ->
                cal.set(Calendar.HOUR, hour)
                cal.set(Calendar.MINUTE, minute)

                val temp = getFormatedDateAndTime(cal.time.time)
                textView.text = temp

            }*/
            val yesDate=System.currentTimeMillis()
            val dialog =DatePickerDialog(
                context,
                R.style.Theme_Holo_Light_Dialog,
                dateSetListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_WEEK_IN_MONTH)
            )
            dialog.datePicker.minDate = yesDate
                dialog.show()
        }
        interface DateTimePickerImpl{
            fun setDateTime(millsec : Long,identifyer:String)
        }
    }
}