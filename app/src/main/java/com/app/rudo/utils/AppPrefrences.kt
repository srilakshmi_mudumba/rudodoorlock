package com.app.rudo.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.app.rudo.model.locklist.Lock
import com.google.gson.Gson

// Created by Satyabrata Bhuyan on 29-06-2020.
// Company  Yutu Electronics
// E_Mail    s.bhuyan0037@gmail.com

private const val NAME = "nick_name"
private const val KEY_SAVED_AT = "key_saved_at"
private const val ACCESSTOKEN = "key_saved_at"
private const val LOCK_DATA = "lock_data"
private const val USER_MOBILE = "mobilenumber"
private const val AUTO_LOCK = "auto_lock_time"
private const val PIN = "pin"

class AppPrefrences(
    context: Context
) {
    private val appContext = context.applicationContext
    private val preference: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(appContext)


    fun savelastSavedAt(savedAt: String) {
        preference.edit().putString(
            KEY_SAVED_AT,
            savedAt
        ).apply()
    }

    fun getLastSavedAt(): String? {
        return preference.getString(KEY_SAVED_AT, null)
    }

    fun saveAccessToken(savedAt: String) {
        preference.edit().putString(
            ACCESSTOKEN,
            savedAt
        ).apply()
    }

    fun getAccessToken(): String? {
        return preference.getString(ACCESSTOKEN, null)
    }

    fun saveLockData(lock: Lock) {
        val gson = Gson()
        val result = gson.toJson(lock).toString()
        preference.edit().putString(
            LOCK_DATA,
            result
        ).apply()
    }

    fun getLockData(): Lock {
        val gson = Gson()
        val pref = preference.getString(LOCK_DATA, "")
        val result = gson.fromJson(pref, Lock::class.java)
        return result
    }

    fun saveUserInfo(mobileNumber: String) {
        preference.edit().putString(
            USER_MOBILE,
            mobileNumber
        ).apply()
    }

    fun getUserInfo(): String? {
        return preference.getString(USER_MOBILE, null)
    }

    fun saveLockAutoTime(lockTime: String) {
        preference.edit().putString(
            AUTO_LOCK,
            lockTime
        ).apply()
    }

    fun getAutoLockTime(): String? {
        return preference.getString(AUTO_LOCK, null)
    }

    fun setPin(pin: String) {
        preference.edit().putString(
            PIN,
            pin
        ).apply()
    }
    fun getPin():String?{
        return preference.getString(PIN, "")
    }

    fun setName(name: String) {
        preference.edit().putString(
            NAME,
            name
        ).apply()
    }
    fun getName():String?{
        return preference.getString(NAME, "")
    }
    fun clear() {
        preference.edit().clear().apply()
    }

}