package com.app.rudo.utils

import com.app.rudo.model.APIError
import java.io.IOException

class ApiException(message: String) : IOException(message)
class NoInternetException(message: String) : IOException(message)
class NoResponseException(message: APIError) : Exception()