package com.app.rudo.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.app.rudo.R

/*
// Created by Satyabrata Bhuyan on 21-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class AleartDialogHelper(context: Context) : BaseDialogHelper() {
    override val dialogView: View by lazy {
        LayoutInflater.from(context).inflate(R.layout.dialog_custom, null)
    }


    override val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        .setView(dialogView)

    //  notes edit text
    val eText: TextView by lazy {
        dialogView.findViewById<TextView>(R.id.txtPasscode)
    }
    val txtMessage: TextView by lazy {
        dialogView.findViewById<TextView>(R.id.txtMessage)
    }

    //  notes edit text
    val bttonOk: Button by lazy {
        dialogView.findViewById<Button>(R.id.btnOk)
    }
    val imgShare: ImageView by lazy {
        dialogView.findViewById<ImageView>(R.id.imgShare)
    }

    //  closeIconClickListener with listener
    fun btnClickListener(func: (() -> Unit)? = null) =
        with(bttonOk) {
            setClickListenerToDialogButton(func)
        }

    fun shareClickListener(func: (() -> Unit)? = null) =
        with(imgShare) {
            setClickListenerToDialogShare(func)
        }
    //  view click listener as extension function
    private fun View.setClickListenerToDialogButton(func: (() -> Unit)?) =
        setOnClickListener {
            func?.invoke()
            dialog?.dismiss()
        }
    private fun View.setClickListenerToDialogShare(func: (() -> Unit)?) =
        setOnClickListener {
            func?.invoke()
            //dialog?.dismiss()
        }
}