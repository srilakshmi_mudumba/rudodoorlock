package com.app.rudo.network.api

import com.app.rudo.BuildConfig
import com.app.rudo.model.ApiResponseModel
import com.app.rudo.network.NetworkConnectionInterceptor
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.POST
import retrofit2.http.Path

/*
// Created by Satyabrata Bhuyan on 22-09-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

interface OtpApi {
    @POST("/API/V1/{api_key}/SMS/+91{phone_no}/AUTOGEN")
    fun sendOtp(
        @Path("api_key") api_key: String?,
        @Path("phone_no") phone_no: String?
    ): Deferred<Response<ApiResponseModel>>?

    @POST("/API/V1/{api_key}/SMS/VERIFY/{session_id}/{otp_entered_by_user}")
    fun verifyOtp(
        @Path("api_key") api_key: String?,
        @Path("session_id") session_id: String?,
        @Path("otp_entered_by_user") otp_entered_by_user: String?
    ): Deferred<Response<ApiResponseModel>>?
    companion object{
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ) : OtpApi{
            var gson = GsonBuilder()
                .setLenient()
                .create()
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            //logging.setLevel(HttpLoggingInterceptor.Level.HEADERS)
            val interceptor =
                Interceptor { chain: Interceptor.Chain ->
                    val original = chain.request()
                    // Request customization: add request headers
                    val requestBuilder = original.newBuilder()
                        .addHeader("Content-Type", "application/x-www-form-urlencoded")
                        .addHeader("Accept", "application/json")
                    val request = requestBuilder.build()
                    chain.proceed(request)
                }

            val okkHttpclient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .addInterceptor(interceptor)
                .addInterceptor(logging)
                .build()

            return Retrofit.Builder()
                .client(okkHttpclient)
                .baseUrl(BuildConfig.OTP_BASE_URL)
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(OtpApi::class.java)
        }
    }
}