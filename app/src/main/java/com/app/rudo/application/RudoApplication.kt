package com.app.rudo.application

import android.app.Application
import com.app.rudo.BuildConfig
import com.app.rudo.network.NetworkConnectionInterceptor
import com.app.rudo.network.api.OtpApi
import com.app.rudo.network.api.RudoApi
import com.app.rudo.repository.AuthRepository
import com.app.rudo.repository.GatewayRepository
import com.app.rudo.repository.HomeRepository
import com.app.rudo.repository.LockDetailRepository
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.view.account.AccountViewModelFactory
import com.app.rudo.view.addlock.AddLockViewmodelFactory
import com.app.rudo.view.auth.AuthViewModelFactory
import com.app.rudo.view.customersupport.SupportViewModelFactory
import com.app.rudo.view.gateway.GatewayViewmodelFactory
import com.app.rudo.view.home.home.HomeViewmodelFactory
import com.app.rudo.view.lockdetails.LockDetailsViewmodelFactory
import com.app.rudo.view.lockdetails.access.GiveAccessViewModelFactory
import com.app.rudo.view.lockdetails.iccard.IccardViewModelFactory
import com.app.rudo.view.lockdetails.passcode.GeneratePassCodeViewmodelFactory
import com.app.rudo.view.settings.SettingViewmodelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import timber.log.Timber

// Created by  on 28-06-2020.
// Company   Yutu Electronics
// E_Mail   s.bhuyan0037@gmail.com

class RudoApplication :Application(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidXModule(this@RudoApplication))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { RudoApi(instance()) }
        bind() from singleton { OtpApi(instance()) }
        bind() from singleton { AppPrefrences(instance()) }
        bind() from singleton { AuthRepository(instance(),instance()) }
        bind() from singleton { GatewayRepository(instance(),instance()) }
        bind() from singleton { LockDetailRepository(instance(),instance()) }
        bind() from provider { AuthViewModelFactory(instance(),instance()) }

        bind() from singleton { HomeRepository(instance(),instance()) }
        bind() from provider { HomeViewmodelFactory(instance(),instance()) }
        bind() from provider { LockDetailsViewmodelFactory(instance()) }
        bind() from provider { GiveAccessViewModelFactory(instance()) }
        bind() from provider { GeneratePassCodeViewmodelFactory(instance()) }
        bind() from provider { AddLockViewmodelFactory(instance(),instance()) }
        bind() from provider { IccardViewModelFactory(instance()) }
        bind() from provider { SettingViewmodelFactory(instance(),instance(),instance()) }
        bind() from provider { GatewayViewmodelFactory(instance(),instance()) }
        bind() from provider { SupportViewModelFactory(instance(),instance()) }
        bind() from provider { AccountViewModelFactory(instance(),instance()) }

    }

    override fun onCreate() {
        super.onCreate()
        initTimber()
        //Fabric.with(this, Crashlytics())
        /* val lockManager: LockManager<CustomPinActivity> = LockManager()
         lockManager.enableAppLock(this, CustomPinActivity::class.java)
         lockManager.appLock.logoId = R.drawable.security_lock*/
    }
    // This will initialise Timber
    fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}