package com.app.rudo.model.lockdetails

import java.io.Serializable

/*
// Created by Satyabrata Bhuyan on 06-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

data class KeyData (
    val keyId:Int?,
    val lockData:String?,
    val lockId:Int?,
    val userType:String?,
    val keyStatus:String?,
    val lockName:String?,
    val lockAlias:String?,
    val lockMac:String?,
    val noKeyPwd:String?,
    val deletePwd:String?,
    val electricQuantity:Int?,
    val lockVersion:LockVersionModel?,
    val startDate:Long?,
    val endDate:Long?,
    val remarks:String?,
    val keyRight:Int?,
    val keyboardPwdVersion:Int?,
    val specialValue:Int?,
    val remoteEnable:Int

):Serializable