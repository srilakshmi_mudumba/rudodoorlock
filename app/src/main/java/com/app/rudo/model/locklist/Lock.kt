package com.app.rudo.model.locklist

import java.io.Serializable

/*
// Created by Satyabrata Bhuyan on 02-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

data class Lock(
    val date: Long?,
    val electricQuantity: Int?,
    val keyboardPwdVersion: Int?,
    var lockAlias: String?,
    val lockId: Int?,
    val noKeyPwd:String?,
    val lockMac: String?,
    val lockName: String?,
    val specialValue: Int?,
    val lockData:String?,
    val userType:String?,	//ekey type:110301-admin ekey, 110302-common user ekey
    val keyId:Int?
):Serializable
