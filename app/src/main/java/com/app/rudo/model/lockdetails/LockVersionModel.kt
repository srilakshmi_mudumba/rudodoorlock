package com.app.rudo.model.lockdetails

import java.io.Serializable

/*
// Created by Satyabrata Bhuyan on 05-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

data class LockVersionModel (
    val protocolType:Int,
    val protocolVersion:Int,
    val scene:Int,
    val groupId:Int,
    val orgId:Int
):Serializable
