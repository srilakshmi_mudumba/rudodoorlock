package com.app.rudo.model

import java.io.Serializable

/*
// Created by Satyabrata Bhuyan on 10-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

data class PassCodeListModel (
    val keyboardPwdId:Int?,
    val lockId:	Int?,
    val keyboardPwd:String?,
    val keyboardPwdName:String?,
    val keyboardPwdVersion:	Int?,
    val keyboardPwdType:Int?,
    val sendDate:Long?,
    val startDate:Long?,
    val endDate:Long?,
    val status:Int?,
    var validityPeriod:String?
):Serializable
