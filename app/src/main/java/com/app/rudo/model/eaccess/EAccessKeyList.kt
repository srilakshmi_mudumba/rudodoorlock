package com.app.rudo.model.eaccess

import com.app.rudo.model.lockdetails.LockVersionModel
import java.io.Serializable

/*
// Created by Satyabrata Bhuyan on 14-08-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

data class EAccessKeyList(
   val keyId:Int?,
   val lockData:String?,
   val lockId:Int?,
   val userType:String?,	//ekey type:110301-admin ekey, 110302-common user ekey
   val keyStatus:String?,
   val lockName:String?,
   var lockAlias:String?,
   val lockMac:String?,
   val noKeyPwd:String?,//	Super passcode, which only belongs to the admin ekey, can be entered on the keypad to unlock
   val deletePwd:String?,//	Erasing passcode,which belongs to old locks, has been abandoned. Please don't use it.
   val electricQuantity:Int?,//Lock battery
   val lockVersion:LockVersionModel?,//	Lock version
   val startDate:Long?,//	The time when it becomes valid
   val endDate:Long?,	//The time when it is expired
   val remarks:String?,
   val keyRight:Int?,//	Is key authorized:0-NO,1-yes
   val keyboardPwdVersion:Int?,//	Passcode version: 0、1、2、3、4
   val specialValue:Int?,	//characteristic value. it is used to indicate what kinds of feature do a lock support.
   val remoteEnable:Int?,	//Is remote unlock enabled: 1-yes,2-no
   val date: Long?
):Serializable

