package com.app.rudo.repository

import com.app.rudo.BuildConfig
import com.app.rudo.model.InitLockModel
import com.app.rudo.model.LockKeyListMode
import com.app.rudo.network.ApiRequest
import com.app.rudo.network.api.RudoApi
import com.app.rudo.utils.AppPrefrences
import java.net.URLDecoder

/*
// Created by Satyabrata Bhuyan on 02-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class HomeRepository(
    private val rudoApi: RudoApi,
    private val appPrefrences: AppPrefrences
) : ApiRequest() {
    suspend fun getLockList(): LockKeyListMode {
        if (BuildConfig.IS_OPENTT_URL_CALL){
            val result = apiRequest {
                rudoApi.getLockListFromOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                1,
                20,
                System.currentTimeMillis()
            )!!
            }
            result?.let {
                result.list?.filter { it.lockAlias!!.isNotEmpty()}?.forEach {
                    val decoded: String = URLDecoder.decode(it.lockAlias, "UTF-8")
                    it.lockAlias = decoded
                }
            }

            return result
        }else{
            val result = apiRequest { rudoApi.getLockList(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                1,
                20,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI +"/v3/lock/list"
            )!!
            }
            result?.let {
                result.list?.forEach {
                    val decoded: String = URLDecoder.decode(it.lockAlias, "UTF-8")
                    it.lockAlias = decoded
                }
            }
            return result
        }

    }

    suspend fun initLock(
        lockData:String,
        aliasName:String
    ):InitLockModel{
        return apiRequest {
            rudoApi.lockInit(
            BuildConfig.CLIENT_ID,
            appPrefrences.getAccessToken(),
            lockData,
            aliasName,
            System.currentTimeMillis(),
            BuildConfig.REDIRECT_OPENTT_URI +"/v3/lock/initialize"
        )!! }
    }
    suspend fun initLockOpenTTApi(
        lockData:String,
        aliasName:String
    ):InitLockModel{
        return apiRequest {
            rudoApi.lockInitOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockData,
                aliasName,
                System.currentTimeMillis()
            )!! }
    }

    suspend fun getEkeyList():LockKeyListMode{
        if(BuildConfig.IS_OPENTT_URL_CALL){
            val result = apiRequest {
                rudoApi.getEkeyListOpenTT(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    1,
                    20,
                    System.currentTimeMillis()
                )!! }
            result?.let {
                result.list?.filter { it.lockAlias!!.isNotEmpty()}?.forEach {
                    val decoded: String = URLDecoder.decode(it.lockAlias, "UTF-8")
                    it.lockAlias = decoded
                }
            }
            return result
        }else{
            val result = apiRequest {
                rudoApi.getEkeyList(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    1,
                    20,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI +"/v3/key/list"
                )!! }
            result?.let {
                result.list?.filter { it.lockAlias!!.isNotEmpty()}?.forEach {
                    val decoded: String = URLDecoder.decode(it.lockAlias, "UTF-8")
                    it.lockAlias = decoded
                }
            }
            return result
        }

    }
}