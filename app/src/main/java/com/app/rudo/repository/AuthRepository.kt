package com.app.rudo.repository

import com.app.rudo.BuildConfig
import com.app.rudo.contrants.Constants
import com.app.rudo.model.ApiResponseModel
import com.app.rudo.model.AuthResponse
import com.app.rudo.network.ApiRequest
import com.app.rudo.network.api.OtpApi
import com.app.rudo.network.api.RudoApi
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.NoInternetException

// Created by Satyabrata Bhuyan on 29-06-2020.
// Company  Yutu Electronics
// E_Mail   s.bhuyan0037@gmail.com

class AuthRepository(
    private val api: RudoApi,
    private val otpApi: OtpApi
) : ApiRequest() {

    suspend fun userLogin(username: String, password: String):AuthResponse {
        try {
            if(BuildConfig.IS_OPENTT_URL_CALL){
                return apiRequest { api.userLoginWithOprnTTApi(
                    BuildConfig.CLIENT_ID,
                    BuildConfig.CLIENT_SECRET,
                    "password",
                    Constants.PREFIX_USERNAME+username,
                    password,
                    BuildConfig.REDIRECT_URI)!! }
            }else{
                return apiRequest { api.userLogin(
                    BuildConfig.CLIENT_ID,
                    BuildConfig.CLIENT_SECRET,
                    "password",
                    Constants.PREFIX_USERNAME+username,
                    password,
                    BuildConfig.REDIRECT_URI,
                    BuildConfig.REDIRECT_OPENTT_URI+"oauth2/token")!! }
            }
        }catch (e:NoInternetException){
            throw e
        }catch (e:ApiException){
            throw e
        }catch (e:Exception){
            throw e
        }
    }

    suspend fun userSignup(
        name: String,
        password: String,
        date: Long
    ) : AuthResponse {
        return apiRequest{ api.userSignup(
            BuildConfig.CLIENT_ID,
            BuildConfig.CLIENT_SECRET,
            name,
            password,
            date,
            BuildConfig.REDIRECT_URI,
            BuildConfig.REDIRECT_OPENTT_URI+"v3/user/register"
        )!!}
    }
    suspend fun userSignupWithOpenTT(
        name: String,
        password: String,
        date: Long
    ) : AuthResponse {
        return apiRequest{ api.userSignupWithOpenTTApi(
            BuildConfig.CLIENT_ID,
            BuildConfig.CLIENT_SECRET,
            name,
            password,
            date,
            BuildConfig.REDIRECT_URI
        )!!}
    }

    suspend fun forgetPassword(username: String, password: String):AuthResponse {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                api.forgotPasswordWothOpenTTApi(
                    BuildConfig.CLIENT_ID,
                    BuildConfig.CLIENT_SECRET,
                    Constants.PREFIX_USERNAME+username,
                    password,
                    System.currentTimeMillis())!! }
        } else {
            return apiRequest { api.forgotPassword(
                BuildConfig.CLIENT_ID,
                BuildConfig.CLIENT_SECRET,
                Constants.PREFIX_USERNAME+username,
                password,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI+"/v3/user/resetPassword")!!
            }
        }

    }

    suspend fun sendOtp(number:String):ApiResponseModel{
        return apiRequest { otpApi.sendOtp(
            BuildConfig.OTP_API_KEY,
            number
        )!! }
    }
    suspend fun verifyOtp(session_id:String,otp:String):ApiResponseModel{
        return apiRequest { otpApi.verifyOtp(
           BuildConfig.OTP_API_KEY,
            session_id,
            otp
        )!! }
    }

    // suspend fun saveUser(user: User) = db.getUserDao().upsert(user)

    //fun getUser() = db.getUserDao().getuser()

}